/** 
Albarran Maria E.
Guillen Elias 
**/

DynList<int> possibles(char *);
char* apply(char*, int, int);
int check(char* square);
int vmax(char* square, int i);
int vmin(char* square, int i);
int minmax(char *square, int i);

int p; 

int minmax(char *square, int i)
{
	int best=13;
	int max_general;
	int max_actual;

	max_general=-100000;

	possibles(square).for_each([&](auto m){
		p=0;
		max_actual= vmin(apply(square, m,i),i); 

		if(max_actual > max_general)
		{
			max_general=max_actual;
			best=m;
		}

	});

	return best;
} 


DynList<int> possibles(char *square)
{
	DynList<int> pos;
	
	for(int i=1; i<26; i++)
	{
		if(square[i]!= 'X' && square[i]!= 'O')
			pos.append(i);
	}
	
	return pos;
}

char* apply(char* square, int pos, int i)
{
	char *square2= new char[26];

	for(int j=0; j<25; j++)
	{
		square2[j]=square[j];
	}

	if(i%2==0)
		square2[pos]= 'O';
	else
		square2[pos]= 'X';

	return square2;
}

int vmin(char* square, int i)
{
	int value;
	i++;
	p++;

	float num = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	if(p==2)
	{
		return (check(square)*num);

	}else{

		value = 100000;
		
		possibles(square).for_each([&](auto m){

			int aux=p;
			value = min(value, vmax(apply(square, m, i),i));
			p=aux;

		});

	}

	return value;
}

int vmax(char* square, int i)
{
	int value;
	i++;
	p++;

	float num = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	if(p==2)
	{
		return (check(square)*num);

	}else{

		value = -100000;
		
		possibles(square).for_each([&](auto m){

			int aux=p;
			value = max(value, vmin(apply(square, m,i),i));
			p=aux;
		});
	}

	return value;
}

int check(char* Square)
{
	int count=0;
	int i;
	int j=1;
	int k;
	int aux_x=0;
	int aux_o=0;
	int aux_vacio=0;

	for(i=1; i<6; i++)
	{
		j=j+5;

		for(k=j-5; k<j; k++)
		{
			if(Square[k] =='X')
			{ 
				aux_x= aux_x+1; 

			}else if(Square[k]=='O'){

				aux_o++;
			}else{
				aux_vacio++;
			}

			if(aux_vacio!=0)
			{
				if(aux_x==0){

					if(aux_o>0)
						count += (aux_x*(80) + (1000/aux_vacio));

					count += (aux_o*20 + (1000/aux_vacio));
				}

				if(aux_o==0){

					count -= (aux_x*(-20) - (1000/aux_vacio));	 
				}
			}


			if(aux_x==4 && aux_vacio==1){
				count += (aux_o*60 + (1000/aux_vacio));
			}

			if(aux_o==4 && aux_vacio==1){
				count += (aux_x*(70) + (1000/aux_vacio));			
			}

		}

		aux_x=0;
		aux_o=0;
		aux_vacio=0;

		for(k=i; k<=(i+(4*5)); k=k+5)
		{
			if(Square[k] =='X')
			{ 
				aux_x++;

			}else if(Square[k]=='O')

				aux_o++;
			else
				aux_vacio++;

			if(aux_vacio!=0)
			{
				if(aux_x==0){

					if(aux_o>0)
						count += (aux_x*(80) + (1000/aux_vacio));

					count += (aux_o*20 + (1000/aux_vacio));
				}

				if(aux_o==0){

					count += (aux_x*(-20) - (1000/aux_vacio));			
				}

			}	



			if(aux_x==4 && aux_vacio==1){
				count += (aux_o*60 + (1000/aux_vacio));
			}

			if(aux_o==4 && aux_vacio==1){
				count += (aux_x*(70) + (1000/aux_vacio));			
			}


		}

		aux_x=0;
		aux_o=0;
		aux_vacio=0;
	}

		for(k=5; k<=21; k=k+4)
		{
			if(Square[k] =='X')
			{ 
				aux_x++;

			}else if(Square[k]=='O')

				aux_o++;
			else
				aux_vacio++;

			if(aux_vacio!=0)
			{
				if(aux_x==0){

					if(aux_o>0)
						count += (aux_x*(80) + (1000/aux_vacio));

					count += (aux_o*20 + (1000/aux_vacio));
				}

				if(aux_o==0){

					count += (aux_x*(-20) - (1000/aux_vacio));			
				}
			}

			if(aux_x==4 && aux_vacio==1){
				count += (aux_o*60 + (1000/aux_vacio));
			}

			if(aux_o==4 && aux_vacio==1){
				count += (aux_x*(70) + (1000/aux_vacio));			
			}

		}

		aux_x=0;
		aux_o=0;
		aux_vacio=0;

		for(k=1; k<=25; k=k+6)
		{
			if(Square[k] =='X')
			{ 
				aux_x++;

			}else if(Square[k]=='O')

				aux_o++;
			else
				aux_vacio++;

			if(aux_vacio!=0)
			{
				if(aux_x==0){

					if(aux_o>0)
						count += (aux_x*(80) + (1000/aux_vacio));

					count += (aux_o*20 + (1000/aux_vacio));
				}

				if(aux_o==0){

					count += (aux_x*(-20) - (1000/aux_vacio));			
				}
			}

			if(aux_x==4 && aux_vacio==1){
				count += (aux_o*60 + (1000/aux_vacio));
			}

			if(aux_o==4 && aux_vacio==1){
				count += (aux_x*(70) + (1000/aux_vacio));			
			}

		}

		aux_x=0;
		aux_x=0;
		aux_vacio=0;

	return count;
}
