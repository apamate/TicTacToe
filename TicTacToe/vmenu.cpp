#include "vmenu.h"
#include "ui_vmenu.h"
#include "vjvsj.h"
#include "vjvscpu.h"
#include "ttt.h"

VMenu::VMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VMenu)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());

    m_pPalette	= new QPalette();
    m_pPixmap = new QPixmap("/home/paola/Ventana2.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);
}

VMenu::~VMenu()
{
    delete ui;
}

void VMenu::on_pb_atras_clicked()
{
    VPrincipal *VMenu = new VPrincipal;
    VMenu->show(); //VMenu a VPrincipal
    this->close();
}

void VMenu::on_pb_jvsj_clicked()
{
    ttt j;
    j.set_mode(1);

    Vjvsj *VMenu = new Vjvsj;
    VMenu->show(); //de v_principal a otra
    this->close();
}

void VMenu::on_pb_jvspc_clicked()
{

    ttt j;
    j.set_mode(2);

    vjvscpu *VMenu = new vjvscpu;
    VMenu->show(); //de mainw a otra
    this->close();
}
