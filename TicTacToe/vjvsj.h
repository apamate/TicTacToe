#ifndef VJVSJ_H
#define VJVSJ_H

#include <QWidget>

namespace Ui {
class Vjvsj;
}

class Vjvsj : public QWidget
{
    Q_OBJECT

public:
    explicit Vjvsj(QWidget *parent = 0);
    ~Vjvsj();

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    void juega(int*, Vjvsj *);

private slots:
    void on_B1_clicked();

    void on_B2_clicked();

    void on_B3_clicked();

    void on_B4_clicked();

    void on_B5_clicked();

    void on_B6_clicked();

    void on_B7_clicked();

    void on_B8_clicked();

    void on_B9_clicked();

    void on_B10_clicked();

    void on_B11_clicked();

    void on_B12_clicked();

    void on_B13_clicked();

    void on_B14_clicked();

    void on_B15_clicked();

    void on_B16_clicked();

    void on_B17_clicked();

    void on_B18_clicked();

    void on_B19_clicked();

    void on_B20_clicked();

    void on_B21_clicked();

    void on_B22_clicked();

    void on_B23_clicked();

    void on_B24_clicked();

    void on_B25_clicked();

    void on_pb_atras_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::Vjvsj *ui;
};

#endif // VJVSJ_H
