/** 
Albarran Maria E.
Guillen Elias 
**/

#include <iostream>
# include <tpl_dynArray.H>
#include "ttt.H"
#include "minmax.H"
#include <stdlib.h>
#include <time.h> 
using namespace std;

int main()
{

	srand(time(NULL));
	char square[26];
	Tictactoe t;
	int player = 1,i,choice, option;
	int turn=0;
	int j=96;
	char mark;

	for(int i=0; i<26; i++)
	{
		square[i]=j;
		j++;
	}

	cout << "TIC TAC TOE" 	<< endl;
	cout << "1. Plaver vs Player" << endl;
	cout << "2. Plaver vs CPU" << endl;
	cin >> option;

	while(option !=1 && option !=2 && option !=3)
	{ 
		cin.clear(); 
  		cin.ignore(numeric_limits<streamsize>::max(),'\n');
		cout << "ERROR, try again: ";
		cin >> option;
	}

	t.set_mode(option);
	t.set_square(square);

	if(option == 1)
	{	
		do
		{
			turn++;
			t.board();
			player=(player%2)?1:2;

			cout << "Player " << player << ", enter a number:  ";
			cin >> choice;

			while(choice < 1  && choice > 25)
			{ 
				cin.clear(); 
		  		cin.ignore(numeric_limits<streamsize>::max(),'\n');
				cout << "ERROR, try again: ";
				cin >> choice;
			}

			mark=(player == 1) ? 'X' : 'O';

			if (square[choice] != 'X' && square[choice] != 'O'){

				square[choice] = mark;

			}else{

				cout<<"Invalid move ";

				player--;
				cin.ignore();
				cin.get();
			}

			t.set_square(square);

			i=t.checkwin();

			player++;
		}while(i==-1);

		t.board();

		if(i==1)
			cout<<"Player "<<--player<<" win ";
		else
			cout<<"Game draw";

		cin.ignore();
		cin.get();

	}else{

		do
		{
			turn++;
			t.board();
			player=(player%2)?1:2;
			t.set_turn(turn);
			
			if(player==1)
			{
				cout << "Player " << player << ", enter a number:  ";
				cin >> choice;

			}else{

				choice = minmax(t.get_square(), turn);			
			}

			mark=(player == 1) ? 'X' : 'O';

			if (square[choice] != 'X' && square[choice] != 'O'){

				square[choice] = mark;

			}else{

				cout<<"Invalid move ";

				player--;
				cin.ignore();
				cin.get();
			}

			t.set_square(square);
			i=t.checkwin();

			player++;

		}while(i==-1);

		t.board();

		if(i==1)
			cout<<"Player "<<--player<<" win ";
		else
			cout<<"Game draw";

		cin.ignore();
		cin.get();
	}

	return 0;
}
