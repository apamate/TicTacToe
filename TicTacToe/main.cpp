#include "vprincipal.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    VPrincipal w;
    w.show();

    return a.exec();
}
