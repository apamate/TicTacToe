#-------------------------------------------------
#
# Project created by QtCreator 2016-12-01T15:33:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TicTacToe
TEMPLATE = app


SOURCES += main.cpp\
        vprincipal.cpp \
    vmenu.cpp \
    utils.cpp \
    vjvsj.cpp \
    vjvscpu.cpp

HEADERS  += vprincipal.h \
    vmenu.h \
    utils.h \
    ttt.h \
    vjvsj.h \
    vjvscpu.h

FORMS    += \
    vprincipal.ui \
    vmenu.ui \
    vjvsj.ui \
    vjvscpu.ui

RESOURCES += \
    recursos.qrc

DISTFILES +=


#Configuracion Aleph

CLANGPATH = /usr/bin

ALEPH = /home/paola/aleph

INCLUDEPATH += $${ALEPH}

LIBS += -L$${ALEPH} -lAleph -lm -lgsl -lgslcblas -lgmp -lmpfr -lasprintf -lpthread -lc

#Configuracion de la linea de compilacion

CONFIG += c++14
QMAKE_CXX = $${CLANGPATH}/clang++-3.8
QMAKE_LINK = $${CLANGPATH}/clang++-3.8
QMAKE_CXXFLAGS_WARN_ON = -Wextra -Wcast-align -Wno-sign-compare -Wno-write-strings -Wno-parentheses -Wno-invalid-source-encoding
QMAKE_CXXFLAGS += -g -O0 -stdlib=libc++

