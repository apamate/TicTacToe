#ifndef UTILS
#define UTILS
#include <tpl_dynArray.H>

char* apply(char*, int, int);
int check(char* square);
int vmax(char* square, int i);
int vmin(char* square, int i);
int minmax(char *square, int i);


#endif // UTILS

