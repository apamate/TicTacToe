#include "vprincipal.h"
#include "ui_vprincipal.h"


VPrincipal::VPrincipal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VPrincipal)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());

    m_pPalette	= new QPalette();
    m_pPixmap = new QPixmap("/home/paola/Principal.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);
}

VPrincipal::~VPrincipal()
{
    delete ui;
}

void VPrincipal::on_pb_np_clicked()
{
    VMenu *VPrincipal = new VMenu;
    VPrincipal->show(); //de v_principal a otra
    this->close();
}
