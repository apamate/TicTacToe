#include "vjvscpu.h"
#include "ui_vjvscpu.h"
#include "ttt.h"
#include "utils.h"
#include "vmenu.h"

char square2[26];
int  player=1;
int turno=0;

vjvscpu::vjvscpu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::vjvscpu)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());

    m_pPalette	= new QPalette();
    m_pPixmap = new QPixmap("/home/paola/Juego.jpg");

    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);


    int j=96;
    for(int i=0; i<26; i++)
    {
        square2[i]=j;
        j++;
    }
}



vjvscpu::~vjvscpu()
{
    delete ui;
}

void vjvscpu::on_B1_clicked()
{
    int option=1;

    ui->B1->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B1->setText("X");

    }else{

       ui->B1->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B2_clicked()
{
    int option=2;
    ui->B2->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B2->setText("X");

    }else{

       ui->B2->setText("O");
    }

    juega(option);
}


void vjvscpu::on_B3_clicked()
{
    int option=3;

    ui->B3->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B3->setText("X");

    }else{

       ui->B3->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B4_clicked()
{
    int option=4;

    ui->B4->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B4->setText("X");

    }else{

       ui->B4->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B5_clicked()
{
    int option=5;

    ui->B5->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B5->setText("X");

    }else{

       ui->B5->setText("O");
    }

    juega(option);
}



void vjvscpu::on_B6_clicked()
{
    int option=6;

    ui->B6->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B6->setText("X");

    }else{

       ui->B6->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B7_clicked()
{
    int option=7;

    ui->B7->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B7->setText("X");

    }else{

       ui->B7->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B8_clicked()
{
    int option=8;

    ui->B8->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B8->setText("X");

    }else{

       ui->B8->setText("O");
    }

    juega(option);
}



void vjvscpu::on_B9_clicked()
{
    int option=9;

    ui->B9->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B9->setText("X");

    }else{

       ui->B9->setText("O");
    }

     juega(option);
}



void vjvscpu::on_B10_clicked()
{
    int option=10;

    ui->B10->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B10->setText("X");

    }else{

       ui->B10->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B11_clicked()
{
    int option=11;

    ui->B11->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B11->setText("X");

    }else{

       ui->B11->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B12_clicked()
{
    int option=12;

    ui->B12->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B12->setText("X");

    }else{

       ui->B12->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B13_clicked()
{
    int option=13;

    ui->B13->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B13->setText("X");

    }else{

       ui->B13->setText("O");
    }

    juega(option);
}



void vjvscpu::on_B14_clicked()
{
    int option=14;

    ui->B14->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B14->setText("X");

    }else{

       ui->B14->setText("O");
    }

    juega(option);
}


void vjvscpu::on_B15_clicked()
{
    int option=15;

    ui->B15->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B15->setText("X");

    }else{

       ui->B15->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B16_clicked()
{
    int option=16;

    ui->B16->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B16->setText("X");

    }else{

       ui->B16->setText("O");
    }

    juega(option);
}



void vjvscpu::on_B17_clicked()
{
    int option=17;

    ui->B17->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B17->setText("X");

    }else{

       ui->B17->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B18_clicked()
{
    int option=18;

    ui->B18->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B18->setText("X");

    }else{

       ui->B18->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B19_clicked()
{
    int option=19;

    ui->B19->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B19->setText("X");

    }else{

       ui->B19->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B20_clicked()
{
    int option=20;

    ui->B20->setEnabled(false);


    if((player%2)!=0)
    {
        ui->B20->setText("X");

    }else{

       ui->B20->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B21_clicked()
{
    int option=21;

    ui->B21->setEnabled(false);


    if((player%2)!=0)
    {
        ui->B21->setText("X");

    }else{

       ui->B21->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B22_clicked()
{
    int option=22;
    ui->B22->setEnabled(false);


    if((player%2)!=0)
    {
        ui->B22->setText("X");

    }else{

       ui->B22->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B23_clicked()
{
    int option=23;
    ui->B23->setEnabled(false);


    if((player%2)!=0)
    {
        ui->B23->setText("X");

    }else{

       ui->B23->setText("O");
    }

    juega(option);
}

void vjvscpu::on_B24_clicked()
{
    int option=24;

    ui->B24->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B24->setText("X");

    }else{

       ui->B24->setText("O");
    }
    juega(option);
}

void vjvscpu::on_B25_clicked()
{
    int option=25;

    ui->B25->setEnabled(false);

    if((player%2)!=0)
    {
        ui->B25->setText("X");

    }else{

       ui->B25->setText("O");
    }

    juega(option);
}

int vjvscpu::juega(int opt){

    ttt t;
    turno++;
    int choice;
    player=(player%2)?1:2;
    t.set_turn(turno);
    char mark;
    int i;

    t.set_square(square2);

    if((player%2)!=0)
    {
        choice=opt;

    }

    mark=(player == 1) ? 'X' : 'O';

    if (square2[choice] != 'X' && square2[choice] != 'O'){

        square2[choice] = mark;

    }

    t.set_square(square2);
    i=t.checkwin();

    if(i==1){

        if(((player-1)%2)==1){
            ui->muestra_ganO->setText("¡Ganador!");
            ui->B1->setEnabled(false);
            ui->B2->setEnabled(false);
            ui->B3->setEnabled(false);
            ui->B4->setEnabled(false);
            ui->B5->setEnabled(false);
            ui->B6->setEnabled(false);
            ui->B7->setEnabled(false);
            ui->B8->setEnabled(false);
            ui->B9->setEnabled(false);
            ui->B10->setEnabled(false);
            ui->B11->setEnabled(false);
            ui->B12->setEnabled(false);
            ui->B13->setEnabled(false);
            ui->B14->setEnabled(false);
            ui->B15->setEnabled(false);
            ui->B16->setEnabled(false);
            ui->B17->setEnabled(false);
            ui->B18->setEnabled(false);
            ui->B19->setEnabled(false);
            ui->B20->setEnabled(false);
            ui->B21->setEnabled(false);
            ui->B22->setEnabled(false);
            ui->B23->setEnabled(false);
            ui->B24->setEnabled(false);
            ui->B25->setEnabled(false);
            return 0;
        }else{
           ui->muestra_ganX->setText("¡Ganador!");
           ui->B1->setEnabled(false);
           ui->B2->setEnabled(false);
           ui->B3->setEnabled(false);
           ui->B4->setEnabled(false);
           ui->B5->setEnabled(false);
           ui->B6->setEnabled(false);
           ui->B7->setEnabled(false);
           ui->B8->setEnabled(false);
           ui->B9->setEnabled(false);
           ui->B10->setEnabled(false);
           ui->B11->setEnabled(false);
           ui->B12->setEnabled(false);
           ui->B13->setEnabled(false);
           ui->B14->setEnabled(false);
           ui->B15->setEnabled(false);
           ui->B16->setEnabled(false);
           ui->B17->setEnabled(false);
           ui->B18->setEnabled(false);
           ui->B19->setEnabled(false);
           ui->B20->setEnabled(false);
           ui->B21->setEnabled(false);
           ui->B22->setEnabled(false);
           ui->B23->setEnabled(false);
           ui->B24->setEnabled(false);
           ui->B25->setEnabled(false);
           return 0;
        }

    }else if(i==0){

        ui->muestra_ganO->setText("¡Empate!");
        ui->muestra_ganX->setText("¡Empate!");
        ui->B1->setEnabled(false);
        ui->B2->setEnabled(false);
        ui->B3->setEnabled(false);
        ui->B4->setEnabled(false);
        ui->B5->setEnabled(false);
        ui->B6->setEnabled(false);
        ui->B7->setEnabled(false);
        ui->B8->setEnabled(false);
        ui->B9->setEnabled(false);
        ui->B10->setEnabled(false);
        ui->B11->setEnabled(false);
        ui->B12->setEnabled(false);
        ui->B13->setEnabled(false);
        ui->B14->setEnabled(false);
        ui->B15->setEnabled(false);
        ui->B16->setEnabled(false);
        ui->B17->setEnabled(false);
        ui->B18->setEnabled(false);
        ui->B19->setEnabled(false);
        ui->B20->setEnabled(false);
        ui->B21->setEnabled(false);
        ui->B22->setEnabled(false);
        ui->B23->setEnabled(false);
        ui->B24->setEnabled(false);
        ui->B25->setEnabled(false);
        return 0;
    }

    player++;

    //turno pc
    turno++;
    choice = minmax(t.get_square(), turno);

    switch(choice){

    case 1:
        ui->B1->setText("O");
        ui->B1->setEnabled(false);
        break;
    case 2:
        ui->B2->setText("O");
        ui->B2->setEnabled(false);
        break;
    case 3:
        ui->B3->setText("O");
        ui->B3->setEnabled(false);
        break;
    case 4:
        ui->B4->setText("O");
        ui->B4->setEnabled(false);
        break;
    case 5:
        ui->B5->setText("O");
        ui->B5->setEnabled(false);
        break;
    case 6:
        ui->B6->setText("O");
        ui->B6->setEnabled(false);
        break;
    case 7:
        ui->B7->setText("O");
        ui->B7->setEnabled(false);
        break;
    case 8:
        ui->B8->setText("O");
        ui->B8->setEnabled(false);
        break;
    case 9:
        ui->B9->setText("O");
        ui->B9->setEnabled(false);
        break;
    case 10:
        ui->B10->setText("O");
        ui->B10->setEnabled(false);
        break;
    case 11:
        ui->B11->setText("O");
        ui->B11->setEnabled(false);
        break;
    case 12:
        ui->B12->setText("O");
        ui->B12->setEnabled(false);
        break;
    case 13:
        ui->B13->setText("O");
        ui->B13->setEnabled(false);
        break;
    case 14:
        ui->B14->setText("O");
        ui->B14->setEnabled(false);
        break;
    case 15:
        ui->B15->setText("O");
        ui->B15->setEnabled(false);
        break;
    case 16:
        ui->B16->setText("O");
        ui->B16->setEnabled(false);
        break;
    case 17:
        ui->B17->setText("O");
        ui->B17->setEnabled(false);
        break;
    case 18:
        ui->B18->setText("O");
        ui->B18->setEnabled(false);
        break;
    case 19:
        ui->B19->setText("O");
        ui->B19->setEnabled(false);
        break;
    case 20:
        ui->B20->setText("O");
        ui->B20->setEnabled(false);
        break;
    case 21:
        ui->B21->setText("O");
        ui->B21->setEnabled(false);
        break;
    case 22:
        ui->B22->setText("O");
        ui->B22->setEnabled(false);
        break;
    case 23:
        ui->B23->setText("O");
        ui->B23->setEnabled(false);
        break;
    case 24:
        ui->B24->setText("O");
        ui->B24->setEnabled(false);
        break;
    case 25:
        ui->B25->setText("O");
        ui->B25->setEnabled(false);
        break;
    }

    mark=(player == 1) ? 'X' : 'O';

    if (square2[choice] != 'X' && square2[choice] != 'O'){

        square2[choice] = mark;

    }

    t.set_square(square2);
    i=t.checkwin();

    if(i==1){

        if(((player)%2)==1){

           ui->muestra_ganX->setText("¡Ganador!");
           ui->B1->setEnabled(false);
           ui->B2->setEnabled(false);
           ui->B3->setEnabled(false);
           ui->B4->setEnabled(false);
           ui->B5->setEnabled(false);
           ui->B6->setEnabled(false);
           ui->B7->setEnabled(false);
           ui->B8->setEnabled(false);
           ui->B9->setEnabled(false);
           ui->B10->setEnabled(false);
           ui->B11->setEnabled(false);
           ui->B12->setEnabled(false);
           ui->B13->setEnabled(false);
           ui->B14->setEnabled(false);
           ui->B15->setEnabled(false);
           ui->B16->setEnabled(false);
           ui->B17->setEnabled(false);
           ui->B18->setEnabled(false);
           ui->B19->setEnabled(false);
           ui->B20->setEnabled(false);
           ui->B21->setEnabled(false);
           ui->B22->setEnabled(false);
           ui->B23->setEnabled(false);
           ui->B24->setEnabled(false);
           ui->B25->setEnabled(false);
           return 0;

        }else{

           ui->muestra_ganO->setText("¡Ganador!");
           ui->B1->setEnabled(false);
           ui->B2->setEnabled(false);
           ui->B3->setEnabled(false);
           ui->B4->setEnabled(false);
           ui->B5->setEnabled(false);
           ui->B6->setEnabled(false);
           ui->B7->setEnabled(false);
           ui->B8->setEnabled(false);
           ui->B9->setEnabled(false);
           ui->B10->setEnabled(false);
           ui->B11->setEnabled(false);
           ui->B12->setEnabled(false);
           ui->B13->setEnabled(false);
           ui->B14->setEnabled(false);
           ui->B15->setEnabled(false);
           ui->B16->setEnabled(false);
           ui->B17->setEnabled(false);
           ui->B18->setEnabled(false);
           ui->B19->setEnabled(false);
           ui->B20->setEnabled(false);
           ui->B21->setEnabled(false);
           ui->B22->setEnabled(false);
           ui->B23->setEnabled(false);
           ui->B24->setEnabled(false);
           ui->B25->setEnabled(false);
           return 0;

        }

    }else if(i==0){

        ui->muestra_ganO->setText("¡Empate!");
        ui->muestra_ganX->setText("¡Empate!");

        ui->B1->setEnabled(false);
        ui->B2->setEnabled(false);
        ui->B3->setEnabled(false);
        ui->B4->setEnabled(false);
        ui->B5->setEnabled(false);
        ui->B6->setEnabled(false);
        ui->B7->setEnabled(false);
        ui->B8->setEnabled(false);
        ui->B9->setEnabled(false);
        ui->B10->setEnabled(false);
        ui->B11->setEnabled(false);
        ui->B12->setEnabled(false);
        ui->B13->setEnabled(false);
        ui->B14->setEnabled(false);
        ui->B15->setEnabled(false);
        ui->B16->setEnabled(false);
        ui->B17->setEnabled(false);
        ui->B18->setEnabled(false);
        ui->B19->setEnabled(false);
        ui->B20->setEnabled(false);
        ui->B21->setEnabled(false);
        ui->B22->setEnabled(false);
        ui->B23->setEnabled(false);
        ui->B24->setEnabled(false);
        ui->B25->setEnabled(false);
        return 0;
    }

    player++;

    return 0;
}


void vjvscpu::on_pb_atras_clicked()
{
    VMenu *Vjvscpu = new VMenu;
    Vjvscpu->show();
    this->close();
}
