#ifndef VMENU_H
#define VMENU_H

#include <QWidget>
#include <vprincipal.h>
#include <vjvsj.h>

namespace Ui {
class VMenu;
}

class VMenu : public QWidget
{
    Q_OBJECT

public:
    explicit VMenu(QWidget *parent = 0);

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    ~VMenu();

private slots:
    void on_pb_atras_clicked();

    void on_pb_jvsj_clicked();

    void on_pb_jvspc_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::VMenu *ui;
};

#endif // VMENU_H
