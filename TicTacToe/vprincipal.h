#ifndef VPRINCIPAL_H
#define VPRINCIPAL_H

#include <QMainWindow>
#include <vmenu.h>

namespace Ui {
class VPrincipal;
}

class VPrincipal : public QMainWindow
{
    Q_OBJECT

public:
    explicit VPrincipal(QWidget *parent = 0);

    void resizeEvent (QResizeEvent* event)
    {
        m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
        setPalette(*m_pPalette);
    };

    ~VPrincipal();

private slots:
    void on_pb_np_clicked();

private:

    QPixmap* m_pPixmap;
    QPalette* m_pPalette;

    Ui::VPrincipal *ui;
};

#endif // VPRINCIPAL_H
